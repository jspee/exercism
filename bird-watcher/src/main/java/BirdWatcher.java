import java.util.Arrays;

class BirdWatcher {
    private final int[] birdsPerDay;
    private static int indexForToday;
    private static final int BUSY_DAY = 5;



    public BirdWatcher(int[] birdsPerDay) {
        this.birdsPerDay = birdsPerDay.clone();
        indexForToday = birdsPerDay.length-1;
    }

    public int[] getLastWeek() {
        return birdsPerDay;
    }

    public int getToday() {
        if (birdsPerDay.length == 0)
            return 0;
        else
            return birdsPerDay[indexForToday];
    }

    public void incrementTodaysCount() {
        birdsPerDay[indexForToday]++;
    }

    public boolean hasDayWithoutBirds() {

        return Arrays.stream(birdsPerDay).filter(value -> value == 0).count() > 0;

    }

    public int getCountForFirstDays(int numberOfDays) {
        if (numberOfDays > birdsPerDay.length) numberOfDays = birdsPerDay.length;
        int totalCount = 0;

        for (int dayIndex = 0; dayIndex < numberOfDays; dayIndex++) {
            totalCount += birdsPerDay[dayIndex];
        }
        return totalCount;
    }

    public int getBusyDays() {
        int busyDayCount = 0;
        for(int dayCount:birdsPerDay) {
            if (dayCount >= BUSY_DAY)
                busyDayCount++;
        }
        return busyDayCount;    }
}

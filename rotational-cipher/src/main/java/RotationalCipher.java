class RotationalCipher {
    private final int ROT;

    RotationalCipher(int shiftKey) {
        ROT = shiftKey;
    }

    String rotate(String input) {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < input.length(); i++){
            output.append(encodeChar(input.charAt(i)));
        }
        return output.toString();
    }

    private char encodeChar(char plainChar){
        final int lowerCaseASCIILowerLimit = 97;
        final int lowerCaseASCIIUpperLimit = lowerCaseASCIILowerLimit+25;
        final int upperCaseASCIILowerLimit = 65;
        final int upperCaseASCIIUpperLimit = upperCaseASCIILowerLimit+25;
        final int plainASCIIPos = (int)plainChar;
        int encodedASCIIPos = plainASCIIPos;

        if ((plainASCIIPos >= lowerCaseASCIILowerLimit) && ((plainASCIIPos <= lowerCaseASCIIUpperLimit)) ){
            encodedASCIIPos = encodeASCII(plainASCIIPos, lowerCaseASCIILowerLimit, lowerCaseASCIIUpperLimit);
        }
        if ((plainASCIIPos >= upperCaseASCIILowerLimit) && ((plainASCIIPos <= upperCaseASCIIUpperLimit)) ){
            encodedASCIIPos = encodeASCII(plainASCIIPos, upperCaseASCIILowerLimit, upperCaseASCIIUpperLimit);
        }
        return (char)encodedASCIIPos;
    }

    private int encodeASCII(int pos, int lowerLimit, int upperLimit){
        int encodedPos = pos+this.ROT;
        if (encodedPos > upperLimit) encodedPos = lowerLimit + (encodedPos-upperLimit) - 1;
        return encodedPos;
    }
}

public class CarsAssemble {
    final double productionRate = 221;
    final int minutesInHour = 60;

    public double productionRatePerHour(int speed) {
        return speed * productionRate * successRate(speed);
    }

    public int workingItemsPerMinute(int speed) {
        return (int) (productionRatePerHour(speed)  / minutesInHour);
    }

    private double successRate(int speed) {
        if (0 < speed && speed < 5) return 1;
        else if (speed < 9) return 0.9;
        else if (speed < 10) return 0.8;
        else if (speed == 10) return 0.77;
        else throw new IllegalArgumentException ("Speed must be between 1 and 10");
    }
}
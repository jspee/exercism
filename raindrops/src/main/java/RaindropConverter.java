class RaindropConverter {

    String convert(int number) {
        String result = "";

        // If the number has 3 as a factor, output 'Pling'.
        if(number % 3 == 0){
            result += "Pling";
        }
        // If the number has 5 as a factor, output 'Plang'.
        if(number % 5 == 0){
            result += "Plang";
        }
        // If the number has 7 as a factor, output 'Plong'.
        if(number % 7 == 0){
            result += "Plong";
        }
        // if the number has no detected factors, return the number
        if (result == ""){
            result = String.valueOf(number);
        }
        return result;
    }

}

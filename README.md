### Java practise ###
Self study [based on the Java track at exercism.io](https://exercism.io/)

### How are the exercises laid out? ###
Each exercise comes wrapped as a gradle project

```
exercise
└───README.md -> exercise instructions
└───src
    └───main   
    │   └───java
    │       └───solution.java -> completed by me
    └───test   
        └───java
            └───solutionTest.java -> provided to show I'd met the requirements
```

class SpaceAge {

    private final double secondsOld;
    private final double secondsInEarthYear = 31557600;
    private final double secondsInMercuryYear = secondsInEarthYear * 0.2408467;
    private final double secondsInVenusYear = secondsInEarthYear * 0.61519726;
    private final double secondsInMarsYear = secondsInEarthYear * 1.8808158;
    private final double secondsInJupiterYear = secondsInEarthYear * 11.862615;
    private final double secondsInSaturnYear = secondsInEarthYear * 29.447498;
    private final double secondsInUranusYear = secondsInEarthYear * 84.016846;
    private final double secondsInNeptuneYear = secondsInEarthYear * 164.79132;

    SpaceAge(double seconds) {
        if (seconds > 0) {
            secondsOld = seconds;
        }
    }

    double onEarth() {
        return ageWhereRotationCycle(secondsInEarthYear);
    }

    double onMercury() {
        return ageWhereRotationCycle(secondsInMercuryYear);
    }

    double onVenus() {
        return ageWhereRotationCycle(secondsInVenusYear);
    }

    double onMars() {
        return ageWhereRotationCycle(secondsInMarsYear);
    }

    double onJupiter() {
        return ageWhereRotationCycle(secondsInJupiterYear);
    }

    double onSaturn() {
        return ageWhereRotationCycle(secondsInSaturnYear);
    }

    double onUranus() {
        return ageWhereRotationCycle(secondsInUranusYear);
    }

    double onNeptune() {
        return ageWhereRotationCycle(secondsInNeptuneYear);
    }

    private double ageWhereRotationCycle(double secondsInPlanetYear) {
        return secondsOld / secondsInPlanetYear;
    }

}

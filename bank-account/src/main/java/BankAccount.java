class BankAccount{

    private int balance = 0;
    private boolean active = false;

    public void open(){
        this.active = true;
    }

    public void close(){
        this.active = false;
    }

    synchronized public void deposit(int depositValue) throws BankAccountActionInvalidException {
        checkValidAccount();
        checkPositiveValue(depositValue);
        this.balance += depositValue;
     }

    synchronized public void withdraw(int withdrawValue) throws BankAccountActionInvalidException {
        checkValidAccount();
        checkPositiveValue(withdrawValue);
        if (this.balance == 0) throw new BankAccountActionInvalidException("Cannot withdraw money from an empty account");
        if ((this.balance - withdrawValue) < 0) throw new BankAccountActionInvalidException("Cannot withdraw more money than is currently in the account");
        this.balance -= withdrawValue;
    }

    synchronized public int getBalance() throws BankAccountActionInvalidException {
        checkValidAccount();
        return this.balance;
    }

    private void checkValidAccount() throws BankAccountActionInvalidException {
        if (!this.active) throw new BankAccountActionInvalidException("Account closed");
    }

    private void checkPositiveValue(int value) throws BankAccountActionInvalidException {
        if (value<0) throw new BankAccountActionInvalidException("Cannot deposit or withdraw negative amount");
    }
}

public class BeerSong {

    public String sing(int startingBeer, int verses) {
        String song = "";

        for (int beer = startingBeer; beer > (startingBeer - verses); beer--) {
            switch (beer) {
                case 2:
                    song = song + "2 bottles of beer on the wall, 2 bottles of beer.\nTake one down and pass it around, 1 bottle of beer on the wall.\n\n";
                    break;
                case 1:
                    song = song + "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n\n";
                    break;
                case 0:
                    song = song + "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n\n";
                    break;
                default:
                    song = song + beer + " bottles of beer on the wall, " + beer + " bottles of beer.\n" + "Take one down and pass it around, " + (beer - 1) + " bottles of beer on the wall.\n\n";
            }
        }

        return song;
    }

    public String singSong() {
        return sing(99, 100);
    }
}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class HandshakeCalculator {

    ArrayList<Signal> signalList = new ArrayList<Signal>();

    List<Signal> calculateHandshake(int number){
        Signal signals[] = Signal.values();
        for (Signal signal : signals)
            updateSignalList(number,signal);
        if (number>=16) Collections.reverse(signalList);
        return signalList;
    }

    void updateSignalList(int number, Signal sig){
        final int VALUE_OF_SIG = (int) Math.pow(2, sig.ordinal());
        if ((number&VALUE_OF_SIG) == VALUE_OF_SIG)
            signalList.add(sig);
    }
}

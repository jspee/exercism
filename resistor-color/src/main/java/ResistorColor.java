import java.util.ArrayList;
import java.util.Arrays;

class ResistorColor {
    private static final ArrayList<String> resistorColours = new ArrayList<>(Arrays.asList("black",
            "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white"));

    int colorCode(String color) {
        return resistorColours.indexOf(color);
    }

    String[] colors() {
        String[] allColours = resistorColours.toArray(new String[resistorColours.size()]);
        return allColours;
    }
}

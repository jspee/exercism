import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Anagram{
    private final String sortedTarget;
    private final String target;

    public Anagram(String target){
        this.target = target.toLowerCase();
        this.sortedTarget = convertToCode(target);
    }

    List match (List<String> candidates){
        ArrayList <String> results = new ArrayList<>();
        for (String word : candidates ) {
            if ((!word.toLowerCase().equals(this.target)) &&
                    (convertToCode(word).equals(this.sortedTarget)))
                results.add(word);
        }
        return results;
    }

    String convertToCode(String sourceText){
        char[] charArray = sourceText.toLowerCase().toCharArray();
        Arrays.sort(charArray);
        return new String(charArray);
    }
<<<<<<< HEAD
}
=======
}
>>>>>>> 72ecf15cfca8ca7b0fc9f4dfcc554ed8b6a92c1f

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

class Robot{
    private String name;
    static Set<String> nameCatalogue = new HashSet<>();

    public Robot(){
        this.name = generateUniqueName();
    }

    public String getName(){
        return this.name;
    }

    public void reset(){
        nameCatalogue.remove(this.name);
        this.name = generateUniqueName();
    }

    private String generateUniqueName(){
        int charA = 65; // A
        int charZ = 90; // Z
        int alphaLength = 2;
        int char0 = 48; // 0
        int char9 = 57; // 9
        int numberLength = 3;
        boolean uniqueNameFound;
        String uniqueName;
        Random random = new Random();

        do {
            uniqueName =
                    random.ints(charA, charZ + 1)
                            .limit(alphaLength)
                            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                            .toString() +
                    random.ints(char0, char9 + 1)
                            .limit(numberLength)
                            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                            .toString();
            uniqueNameFound =nameCatalogue.add(uniqueName);
        }
        while (!uniqueNameFound);
        return uniqueName;
    }

}
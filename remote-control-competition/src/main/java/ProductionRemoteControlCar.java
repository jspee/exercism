class ProductionRemoteControlCar implements RemoteControlCar {
    private final int distanceDrivenOneCycle = 10;
    private int distanceTravelled = 0;
    private int victoryCount = 0;

    public void drive() {
        distanceTravelled += distanceDrivenOneCycle;
    }

    public int getDistanceTravelled() {
        return this.distanceTravelled;
    }
    public int getNumberOfVictories() {
        return this.victoryCount;
    }

    public void setNumberOfVictories(int numberOfVictories) {
        this.victoryCount = numberOfVictories;
    }
}

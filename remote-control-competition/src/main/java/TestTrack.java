import java.util.*;
import java.util.stream.Collectors;

public class TestTrack {

    public static void race(RemoteControlCar car) {
        car.drive();
    }

    public static List<ProductionRemoteControlCar> getRankedCars(ProductionRemoteControlCar prc1,
                                                                 ProductionRemoteControlCar prc2) {

        List<ProductionRemoteControlCar> rankings = Arrays.asList(prc1, prc2);
        Comparator<ProductionRemoteControlCar> carComparatorLambda =
                (car1, car2) -> Integer.valueOf(car1.getNumberOfVictories()).compareTo(car2.getNumberOfVictories());
        Collections.sort(rankings, carComparatorLambda);
        return rankings;
    }
}

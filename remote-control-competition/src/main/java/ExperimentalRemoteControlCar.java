public class ExperimentalRemoteControlCar implements RemoteControlCar{
    private final int distanceDrivenOneCycle = 20;
    private int distanceTravelled = 0;

    public void drive() {
        distanceTravelled += distanceDrivenOneCycle;
    }

    public int getDistanceTravelled() {
        return this.distanceTravelled;
    }

}
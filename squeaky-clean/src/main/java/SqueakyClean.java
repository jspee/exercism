import java.util.regex.Pattern;
import java.util.regex.Matcher;

class SqueakyClean {
    static String clean(String identifier) {

        if (identifier.isEmpty()) return ("");
        return kebabCaseHandler(identifier)
                .replace(" ", "_")
                .toString().replace("-", "")
                .replaceAll("[\\p{Cntrl}\\p{Cc}]", "CTRL")
                .replaceAll("[\\d]", "")
                .replaceAll("(\\uD83D\\uDE00)", "")
                .replaceAll("[\\u03B1-\\u03C9]", "");
    }

    private static String kebabCaseHandler(String identifier) {
        Pattern kebabPattern = Pattern.compile("(-\\p{Ll})");
        Matcher kebabMatcher = kebabPattern.matcher(identifier);
        int lastIndex = 0;
        StringBuilder cleanedString = new StringBuilder();

        while (kebabMatcher.find()) {
            cleanedString.append(identifier, lastIndex, kebabMatcher.start())
                    .append(kebabMatcher.group(1).toUpperCase());
            lastIndex = kebabMatcher.end();
        }
        if (lastIndex < identifier.length()) {
            cleanedString.append(identifier, lastIndex, identifier.length());
        }
        return cleanedString.toString();
    }
}


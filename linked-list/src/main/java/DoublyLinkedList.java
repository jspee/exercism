public class DoublyLinkedList <T>{

    private Node<T> origin;
    private Node<T> current;

    public DoublyLinkedList(){
    }

    public void push(T newNodeValue){
        if (origin == null){
            current = new Node<>(newNodeValue);
            origin = current;
        }
        else {
            Node<T> newNode = new Node<>(newNodeValue);
            current.setNext(newNode);
            newNode.setPrevious(current);
            current = newNode;
        }
    }

    public T pop(){
        T tempValue = current.getValue();
        current = current.getPrevious();
        return tempValue;
    }

    public void unshift(T newNodeValue){
        if (origin == null){
            current = new Node<>(newNodeValue);
            origin = current;
        }
        else{
            Node<T> newNode = new Node<>(newNodeValue);
            origin.setPrevious(newNode);
            newNode.setNext(origin);
            origin = newNode;
        }
    }

    public T shift(){
        T tempValue = origin.getValue();
        origin = origin.getNext();
        return tempValue;
    }
}


public class Node <T> {
    private T value = null;
    private Node <T> nextNode = null;
    private Node <T> previousNode = null;

    public Node(T newNodeValue) { this.value = newNodeValue; }

    public void setPrevious (Node<T> previousNode){
        this.previousNode = previousNode;
    }

    public void setNext (Node<T> nextNode){
        this.nextNode = nextNode;
    }

    public T getValue (){
        return this.value;
    }

    public Node<T> getPrevious (){
        return this.previousNode;
    }

    public Node<T> getNext (){
        return this.nextNode;
    }
}

import java.util.List;

class BinarySearch {

    private final List<Integer> myList;

    public BinarySearch(List<Integer> list){
        myList = list;
    }
    
    public int indexOf(int target) throws ValueNotFoundException {
        int location;
        if (this.myList.size() == 0)
            throw new ValueNotFoundException("Value not in array");
        else{
            location = search(target);
        }
        return location;
    }

    private int search(int target) throws ValueNotFoundException {
        int location = -1;
        int searchLowPoint = 0;
        int searchHighPoint = this.myList.size();
        int searchPoint = searchHighPoint/2;

        while (location == -1) {
            if (this.myList.get(searchPoint) == target)
                location = searchPoint;
            else {
                if ( searchLowPoint == searchHighPoint-1 ) {
                    // target not in array
                    throw new ValueNotFoundException("Value not in array");
                }
                else {
                    // update search limits
                    if (target < this.myList.get(searchPoint)) {
                        searchHighPoint = searchPoint;
                        searchPoint = (searchLowPoint + searchPoint) / 2;
                    } else {
                        searchLowPoint = searchPoint;
                        searchPoint = (searchHighPoint + searchPoint) / 2;
                    }
                }
            }
        }
        return location;
    }
}

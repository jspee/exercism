import java.util.*;

class Sieve {
    static int maxRange = 0;
    Sieve(int maxPrime) {
        maxRange = maxPrime;
    }

    List<Integer> getPrimes() {
        List<Integer> primes = new ArrayList<>();
        Boolean[] potentialPrimes = new Boolean[maxRange+1];
        Arrays.fill(potentialPrimes, true);

        for(int i = 2; i< potentialPrimes.length; i++){
            if (potentialPrimes[i]) primes.add(i);
            for(int primeDetection = i*2; primeDetection< potentialPrimes.length; primeDetection+=i){
                potentialPrimes[primeDetection]=false;
            }
        }
        return primes;
    }
}
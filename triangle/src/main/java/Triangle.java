class Triangle {
    private boolean isosceles, equilateral, scalene;

    Triangle(double side1, double side2, double side3) throws TriangleException {

        if (triangleIsValid(side1, side2, side3)){
            if (side1==side2 && side2==side3) equilateral=true;
            if ((side1==side2) || (side2==side3) || (side3==side1)) isosceles =true;
            if (!equilateral && !isosceles) scalene = true;
        }
        else {
            throw new TriangleException();
        }
    }

    boolean isEquilateral() {
        return (equilateral);
    }

    boolean isIsosceles() {
        return (isosceles);
    }

    boolean isScalene() {
        return (scalene);
    }

    private boolean triangleIsValid(double side1, double side2, double side3) {
        boolean valid = true;
        if (side1==0 || side2==0 || side3==0)
            valid = false;
        if ( ( side1+side2 < side3) || ( side2+side3 < side1) || ( side1+side3 < side2) )
            valid = false;
        return valid;
    }
}

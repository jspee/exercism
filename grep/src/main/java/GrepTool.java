import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Set;

public class GrepTool {
    // no more instance variables
    public String grep (String searchTarget, List<String> flags, List<String> searchFiles) {
        GrepState grepState = new GrepState(flags, searchFiles.size()); // added an object to hold state
        for(String currentFile : searchFiles){
            try {
                List<String> result = Files.readAllLines(Paths.get(currentFile)); // Q - is redeclaring result, best practise?
                ListIterator fileLine = result.listIterator();
                while (fileLine.hasNext()) {  // each line in a file
                    String currentFileLine = fileLine.next().toString();
                    if (stringsMatch(searchTarget, currentFileLine, grepState))
                        grepState.appendResult(currentFileLine, fileLine.nextIndex(), currentFile);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (grepState.FILENAME_RESULTS) return String.join("\n", grepState.fileResults);
        else return String.join("\n", grepState.textResults);
    }

    private boolean stringsMatch(String target, String source, GrepState grepState) {
        boolean result;
        if (grepState.CASE_INSENSITIVE) {
            target = target.toLowerCase(Locale.ROOT);
            source = source.toLowerCase(Locale.ROOT);
        }
        if (grepState.MATCH_FULL_LINE) {
            result = source.equals(target);
        } else {
            result = source.contains(target);
        }
        if (grepState.RETURN_FAILING_LINES) result = !result;
        return result;
    }
}

class GrepState{
    final boolean INCLUDE_LINE_NUM, FILENAME_RESULTS, CASE_INSENSITIVE, RETURN_FAILING_LINES, MATCH_FULL_LINE, MULTIPLE_SEARCH_FILES;

    ArrayList<String> textResults = new ArrayList<>();
    Set<String> fileResults = new HashSet<>();

    public GrepState (List<String> commandFlags, int filesToSearch){
        INCLUDE_LINE_NUM = commandFlags.contains("-n");  // meaningful flag names
        FILENAME_RESULTS = commandFlags.contains("-l");
        CASE_INSENSITIVE = commandFlags.contains("-i");
        RETURN_FAILING_LINES = commandFlags.contains("-v");
        MATCH_FULL_LINE = commandFlags.contains("-x");
        MULTIPLE_SEARCH_FILES = (filesToSearch > 1);
    }

    void appendResult(String fileLine, int lineNumber, String filename) {
        String newText;
        fileResults.add(filename);
        if (INCLUDE_LINE_NUM)
            newText = lineNumber + ":" + fileLine;
        else
            newText = fileLine;
        if (MULTIPLE_SEARCH_FILES)
            newText = filename + ":" + newText;
        textResults.add(newText);
    }
}


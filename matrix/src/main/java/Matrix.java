

class Matrix {
    private int [][]matrixAsInteger;
    private final int totalRows;
    private final int totalColumns;

    Matrix(String matrixAsString) {
        String[] rowsAsString = matrixAsString.split("\n");
        totalRows = rowsAsString.length;
        totalColumns = rowsAsString[0].split(" ").length;
        matrixAsInteger = new int[totalRows][totalColumns];

        String[] rowElementsAsString;
        for (int row = 0; row < totalRows; row++){
            rowElementsAsString = rowsAsString[row].split(" ");
            for (int col = totalColumns-1; col >= 0; col--){
                matrixAsInteger[row][col] = Integer.parseInt(rowElementsAsString[col]);
            }
        }
    }

    public int[] getRow(int rowNumber) {
        return matrixAsInteger[rowNumber - 1];
    }

    public int[] getColumn(int columnNumber) {
        int[] resultsArray = new int[totalRows];
        for (int i = 0; i< totalRows; i++)
            resultsArray[i] = matrixAsInteger[i][columnNumber-1];
        return resultsArray;
    }
}

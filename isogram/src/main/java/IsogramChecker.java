class IsogramChecker {

    boolean isIsogram(String phrase) {

        // remove non alpha, and convert to lowercase
        phrase = phrase.replaceAll("[^a-zA-Z]", "");
        phrase = phrase.toLowerCase();
        // controls
        Integer phraseLen = phrase.length();
        Boolean isogramPossible = true;
        Integer charPos = 0, remainingCharPos = 0;
        Character currentCharacter = null;

        // loop through each char
        while (isogramPossible && charPos<phraseLen){
            currentCharacter = phrase.charAt(charPos);
            remainingCharPos = charPos+1;
            // compare that char with remaining part of the string
            while(isogramPossible && remainingCharPos<phraseLen) {
                if (phrase.charAt(remainingCharPos) == currentCharacter) isogramPossible = false;
                remainingCharPos++;
            }
            charPos++;
        }
        return isogramPossible;
    }

}

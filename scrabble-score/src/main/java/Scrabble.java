import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

class Scrabble {

    int wordScore = 0;
    HashMap<Character, Integer> scoreMap = new HashMap<>();

    Scrabble(String word) {
        initialiseScoreMap();
        word = word.toLowerCase();

        for (int i = 0; i < word.length(); i++) {
            wordScore += scoreMap.get(word.charAt(i));
        }
    }

    int getScore() {
        return wordScore;
    }

    void initialiseScoreMap() {
        // score 1
        scoreMap.put('a', 1);
        scoreMap.put('e', 1);
        scoreMap.put('i', 1);
        scoreMap.put('o', 1);
        scoreMap.put('u', 1);
        scoreMap.put('l', 1);
        scoreMap.put('n', 1);
        scoreMap.put('r', 1);
        scoreMap.put('s', 1);
        scoreMap.put('t', 1);
        // score 2
        scoreMap.put('d', 2);
        scoreMap.put('g', 2);
        //score 3
        scoreMap.put('b', 3);
        scoreMap.put('c', 3);
        scoreMap.put('m', 3);
        scoreMap.put('p', 3);
        // score 4
        scoreMap.put('f', 4);
        scoreMap.put('h', 4);
        scoreMap.put('v', 4);
        scoreMap.put('w', 4);
        scoreMap.put('y', 4);
        // score 5
        scoreMap.put('k', 5);
        // score 8
        scoreMap.put('j', 8);
        scoreMap.put('x', 8);
        // score 10
        scoreMap.put('q', 10);
        scoreMap.put('z', 10);
    }

}

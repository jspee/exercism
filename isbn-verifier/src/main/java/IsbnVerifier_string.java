class IsbnVerifier_string {

    boolean isValid(String stringToVerify) {
        boolean result;
        int charValueToVerify;
        int valueSum = 0;
        String cleanedString = "";
        if (stringToVerify.length() < 10)
            return false;
        // clean string
        stringToVerify = stringToVerify.toUpperCase();
        for (int i = 0; i < stringToVerify.length() - 1; i++) { // remove non numberics
            charValueToVerify = stringToVerify.charAt(i);
            if (charValueToVerify >= 48 && charValueToVerify <= 57) {
                cleanedString += (char)charValueToVerify;
            }
        }
        charValueToVerify = stringToVerify.charAt(stringToVerify.length() - 1); // allow X or number at final position only
        if( (charValueToVerify >= 48 && charValueToVerify <= 57) || charValueToVerify == 88 ){
            cleanedString += (char)charValueToVerify;
        }

        if (cleanedString.length() != 10)
            return false;

        for (int i = 0; i < 10; i++) { // remove non numberics
            valueSum += charMappedValue(cleanedString.charAt(i)) * (10-i);
        }

        if (valueSum % 11 == 0)
            return true;
        else
            return false;
    }

    private int charMappedValue(char letter){
        int value;
        if (letter >= 48 && letter <= 57)
            value = letter-48;
        else
            value=10;
        return value;
        
    }
}
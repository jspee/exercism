import java.util.regex.Matcher;
import java.util.regex.Pattern;
class IsbnVerifier {
    boolean isValid(String input) {
        if(isValidFormat(input)) {
            return isISBN(convertToArray(input));
        }
        else {return false;}
    }
    boolean isValidFormat(String stringToVerify){
        Pattern p = Pattern.compile("\\b\\d-?\\d{3}-?\\d{5}-?[X0-9]\\b");
        Matcher m = p.matcher(stringToVerify);
        return m.matches();
    }
    int[] convertToArray(String input){
        int[] output = input.chars()
                .filter(c -> c != '-')
                .map(c -> (c == 'X') ? 10 : Character.getNumericValue(c))
                .toArray();
        return output;
    }
    boolean isISBN(int[] array){
        int counter = 10;
        int isbnProduct = 0;
        for(int i=0; i<array.length; i++ ){
            isbnProduct += counter * array[i];
            counter--;
        }
        return (isbnProduct%11 == 0);
    }
}
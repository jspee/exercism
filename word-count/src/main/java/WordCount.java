import java.util.HashMap;
import java.util.Map;

public class WordCount{

    public Map phrase (String phrase){
        phrase = stringCleaner(phrase);
        Integer count;
        Map<String, Integer> wordDistribution = new HashMap<>();
        String[] wordSet = phrase.split("\\s+");
        for (String word : wordSet) {
            count = wordDistribution.get(word);
            if (count == null) wordDistribution.put(word, 1);
            else wordDistribution.put(word, count + 1);
        }
        return wordDistribution;
    }

    private String stringCleaner(String dirty){
        String clean = dirty
                .replaceAll("[.:!&@$%^&]|[']$|\n", "")
                .replaceAll("' | '|,", " ")
                .toLowerCase()
                .trim();

        return clean;
    }
}

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Gigasecond {
    private Duration d = Duration.ofSeconds(1000000000);
    private LocalDateTime gigaTime;

    public Gigasecond(LocalDate moment) {
        gigaTime = moment.atStartOfDay();
        gigaTime = gigaTime.plus(d);
    }

    public Gigasecond(LocalDateTime moment) {
        gigaTime = moment.plus(d);
    }

    public LocalDateTime getDateTime() {
        return gigaTime;
    }
}

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
class Matrix {
    Set<MatrixCoordinate> saddlePoints = new HashSet<>();

    Matrix(List<List<Integer>> values) {
        if (values.size()==0)
            saddlePoints = Collections.emptySet();
        else {
            int numberRows = values.size();
            int numberCols = values.get(0).size();
            boolean saddlePointFound;
            for (int row = 0; row < numberRows; row++) { // run through each row
                int biggestRowValue = Collections.max(values.get(row));
                for (int col = 0; col < numberCols; col++) { // horizontal scan
                    if (values.get(row).get(col) == biggestRowValue) {
                        saddlePointFound = true;
                        int rowScan = 0;
                        while (saddlePointFound && rowScan < numberRows) { // vertical scan
                            if (values.get(rowScan).get(col) < biggestRowValue) {
                                saddlePointFound = false;
                            }
                            rowScan++;
                        }
                        if (saddlePointFound)
                            saddlePoints.add(new MatrixCoordinate(row + 1, col + 1));
                    }
                }
            }
        }
    }

    Set<MatrixCoordinate> getSaddlePoints() {
        return this.saddlePoints;
    }
}
